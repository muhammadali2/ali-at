from selenium import webdriver
# from selenium.webdriver.support import expected_conditions as EC
import unittest
import time

from Pages import LoginPage
from locator import Locators
from Testdata import TestData


# from Pages import LoginPage
class BaseTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()

    def tearDown(self):
        print("close")
        # self.driver.close()
        # self.driver.quit()


class LoginTest(BaseTest):

    # def test_email_valid(self):


    def test_email_invalid(self):
        self.login_page = LoginPage(self.driver)
        self.login_page.email_invalid()
    # assert
    #     alert_email = self.login_page.get_text(Locators.ALERT_EMAIL_INVALID)
    #     self.assertEqual(TestData.ALERT_EMAIL_INVALID_DATA, alert_email)
    #     time.sleep(4)

    # def test_email_password_valid(self):
    #     # Load Login
    #     self.login_page = LoginPage(self.driver)
    #     # Execute code
    #     self.login_page.login_valid()
    #     # assert
    #     register_info = self.login_page.get_text(Locators.TOAST_LOGIN_SUCCESS)
    #     self.assertEqual(TestData.TOAST_LOGIN_SUCCESS_DATA, register_info)
    #     time.sleep(3)
################################################
    def test_login_valid(self):
        self.login_page = LoginPage(self.driver)
        self.login_page.login_valid()
################################################

# Test script untuk execution test
# Button Tour Product

    def test_tour_buttonnav(self):
        self.klik_buttonnav = LoginPage(self.driver)
        self.klik_buttonnav.click_navigasi_tour()
        time.sleep(4)

######################button#############################
    # Filter product tour with valid data

    def test_filter_tour_byproductname_valid(self):
        self.filter_tour_product = LoginPage(self.driver)
        self.filter_tour_product.filter_by_productname()
        time.sleep(6)

    def test_filter_tour_bycategory_valid(self):
        self.filter_tour_product = LoginPage(self.driver)
        self.filter_tour_product.filter_tour_productcategory()
        time.sleep(6)

    def test_filter_tour_bydestination_valid(self):
        self.filter_by_de = LoginPage(self.driver)
        self.filter_by_de.filter_bydestination()
        time.sleep(6)

    def test_filter_tour_bystatusprod_valid(self):
        self.filter_tour_product = LoginPage(self.driver)
        self.filter_tour_product.filter_tour_productstatus()
        time.sleep(6)

    def test_filter_tour_bystatuslive_valid(self):
        self.filter_tour_product = LoginPage(self.driver)
        self.filter_tour_product.filter_tour_statuslive()
        time.sleep(4)

    # Filter product tour with invalid data
    def test_filter_byname_invalid(self):
        self.filter_tour_invalid = LoginPage(self.driver)
        self.filter_tour_invalid.filter_byname_invalid()
        time.sleep(3)
        register_info = self.filter_tour_invalid.get_text(Locators.ALERT_DATA_INVALID)
        self.assertEqual(TestData.ALERT_NAMEPRODUCT_INVALID, register_info)
        time.sleep(4)

    def test_filter_bycategory_invalid(self):
        self.filter_bycategry_invalid = LoginPage(self.driver)
        self.filter_bycategry_invalid.filter_bycategory_invalid()
        time.sleep(3)
        register_info = self.filter_bycategry_invalid.get_text(Locators.ALERT_DATA_INVALID)
        self.assertEqual(TestData.ALERT_CATEGORY_INVALID, register_info)
        time.sleep(4)

    def test_filter_bycdestination_invalid(self):
        self.filter_bydest_invalid = LoginPage(self.driver)
        self.filter_bydest_invalid.filter_bydestination_invalid()
        time.sleep(2)
        register_info = self.filter_bydest_invalid.get_text(Locators.ALERT_DATA_INVALID)
        self.assertEqual(TestData.ALERT_DESTINATION_INVALID, register_info)
        time.sleep(4)

    def test_filter_by_invaliddata(self):
        self.filter_bydata_invalid = LoginPage(self.driver)
        self.filter_bydata_invalid.filter_producttour_invaliddata()
        time.sleep(2)
        register_info = self.filter_bydata_invalid.get_text(Locators.ALERT_DATA_INVALID)
        self.assertEqual(TestData.ALERT_DATA_INVALID, register_info)
        time.sleep(4)

    # Filter merchant user
    def test_filter_merchant_user_valid(self):
        self.filter_usr = LoginPage(self.driver)
        time.sleep(2)
        self.filter_usr.filter_merchant()
        time.sleep(3)

    # Filter merchant user with invalid data
    def test_filter_user_invalid_name(self):
        self.filter_usr_invalid = LoginPage(self.driver)
        time.sleep(2)
        self.filter_usr_invalid.filter_merchant_invalid_name()
        time.sleep(3)

    def test_filter_user_invalid_email(self):
        self.filter_usr_invalid = LoginPage(self.driver)
        time.sleep(2)
        self.filter_usr_invalid.filter_merchant_invalid_email()
        time.sleep(3)

    # Create merchant user with valid data
    def test_createnew_user_valid(self):
        self.add_usrmerchant = LoginPage(self.driver)
        self.add_usrmerchant.add_usermerchant()
        time.sleep(2)

    # Create merchant user invalid
    def test_invalid_notselect_merchantname(self):
        self.add_usr_invalid = LoginPage(self.driver)
        self.add_usr_invalid.add_usermerchant_invalid()
        register_info = self.add_usr_invalid.get_text(Locators.ALERT_MERCHANTNAME_NOSELECT)
        self.assertEqual(TestData.ALERT_MERCHANTNAME_NOSELECT, register_info)
        time.sleep(2)

    def test_fullname_empty(self):
        self.add_fullname_empty = LoginPage(self.driver)
        self.add_fullname_empty.add_withfullname_empty()
        register_info = self.add_fullname_empty.get_text(Locators.ALERT_FULLNAME_EMPTY)
        self.assertEqual(TestData.ALERT_FULLNAME_EMPTY, register_info)
        time.sleep(2)

    def test_email_empty(self):
        self.add_email_empty = LoginPage(self.driver)
        self.add_email_empty.add_withemail_empty()
        register_info = self.add_email_empty.get_text(Locators.ALERT_EMAILUSER_EMPTY)
        self.assertEqual(TestData.ALERT_EMAILUSER_EMPTY, register_info)
        time.sleep(2)

    def test_emailuser_invalid(self):
        self.add_email_invalid = LoginPage(self.driver)
        self.add_email_invalid.add_withemail_invalid()
        register_info = self.add_email_invalid.get_text(Locators.ALERT_EMAILUSER_INVALID)
        self.assertEqual(TestData.ALERT_EMAILUSER_INVALID, register_info)
        time.sleep(2)

    def test_notselect_role(self):
        self.add_notselect_role = LoginPage(self.driver)
        self.add_notselect_role.add_withenotselect_role()
        register_info = self.add_notselect_role.get_text(Locators.ALERT_ROLE_NOSELECT)
        self.assertEqual(TestData.ALERT_ROLE_NOSELECT, register_info)
        time.sleep(2)


    def test_emailwas_exist(self):
        self.add_emailwas_exist = LoginPage(self.driver)
        self.add_emailwas_exist.add_withemail_exist()


    # List user merchant
    def test_list_user(self):
        self.list_usr = LoginPage(self.driver)
        self.list_usr.list_usermerchant()
        time.sleep(4)


    # Add product tour dengan login user merchant
    # date picker harus disesuikan dulu sebelum running
    def test_add_product_tour(self):
        self.add_prd = LoginPage(self.driver)
        self.add_prd.create_tour_product()
        time.sleep(4)


    # Add product tour via link admin misteraladin
    # date picker harus disesuaikan dulu sebelum running
    def test_add_product_tour_viama(self):
        self.add_prd = LoginPage(self.driver)
        self.add_prd.create_tour_via_ma()
        time.sleep(2)

    # View product tour by merchant
    def test_view_product_tour(self):
        self.view_prd = LoginPage(self.driver)
        self.view_prd.view_producttour()
        time.sleep(3)

    def test_list_product_tour(self):
        self.list_prd = LoginPage(self.driver)
        self.list_prd.list_product_tour()
        time.sleep(3)

    # View product tour by admin misteraladin
    def test_view_product_tour_viama(self):
        self.view_prd_ma = LoginPage(self.driver)
        self.view_prd_ma.view_producttour_viama()
        time.sleep(3)
    # edit product macet di corse
    def test_edit_product_tour(self):
        self.edit_prd = LoginPage(self.driver)
        self.edit_prd.edit_tour_via_ma()
        time.sleep(3)