from faker import Faker


class TestData:
    fake = Faker(["id_ID"])
    # base url untuk user merchant
    # BASE_URL = "https://staging-partner-explore.misterb2b.com/login"
    # base url untuk admin ma
    BASE_URL = "https://staging-partner-explore.misterb2b.com/?appId=121001&appSecret=d300a6359bff9592982a6ac996dab4ec&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJpY2t5Lmd1bmF3YW5AbWlzdGVyYWxhZGluLmNvbSIsImV4cCI6MTY1ODE5ODMyNSwiaWF0IjoxNjI2NjYyMzI1LCJpc3MiOiJtaXN0ZXJhbGFkaW4uY29tIiwibmFtZSI6IlJpY2t5IiwibmJmIjoxNjI2NjYyMzI1LCJyb2xlIjoiYWRtaW5pc3RyYXRvciIsInN1YiI6N30.tNcQCXa5RcY673OYiQ76IfaTDBQ43TnZEy9DufZc4R4"
    # data untuk login
    SIGN_IN_EMAIL = 'qarefactory@gmail.com'
    SIGN_IN_EMAIL_INVALID = 'misteraladinqa_gmail.com'
    # data untuk login invalid
    PASSWORD_DATA = '@Misteraa'
    SIGN_INVALID = 'XHSFVKSDGF'
    ALERT_EMAIL_INVALID_DATA = "That email address isn't correct"
    # data untuk klik tour button navigasi
    # TOUR_BUTTON
    # data test filter
    PRODUCT_NAME_FIELD = 'Ekplorasi keindahan alam '
    CATEGORY_FIELD = 'Liburan Aman'
    DESTINATION_FIELD = 'Open Trip'
    STATUS_PRODUCT_DROPDWN = 'Need Approval'
    STATUS_LIVE_DROPDWN = 'Draft'
    PRODUCT_NAME_FIELD_INVALID = 'aabba'
    CATEGORY_FIELD_INVALID = 'aabbb'
    DESTINATION_FIELD_INVALID = 'aabb'
    ALERT_DATA_INVALID = 'Result not found'
    ALERT_DESTINATION_INVALID = 'Result not found'
    ALERT_CATEGORY_INVALID = 'Result not found'
    ALERT_NAMEPRODUCT_INVALID = 'Result not found'




    ############################################## data create tour product ####################################
    # title
    TITLE_INDO_FIELD = 'Menjelajah Sungai Pinus'
    TITLE_ENG_FIELD = 'Explore river forests'
    # detail
    REGION = 'International'
    REGION_EDIT = 'Domestic'
    DESTINATION = 'Aceh'
    CATEGORY = 'Concert Ticket'
    TAGS = 'Duration: 8 hours'
    CONDITION = 'With adult is a must'
    # booking
    BOOKING_DW = '4'
    TIME_JAM = '20'
    TIME_MENIT = '20'
    TIME_JAM_EDIT = '12'
    TIME_MENIT_EDIT = '00'
    TIME_JAMEND = '00'
    TIME_MENITEND = '11'
    TAB_INDO_TITFIELD = 'Paket hemat'
    TAB_INDO_TITFIELD_EDIT = 'PAKET MURAH BGT'
    TAB_ENG_TITFIELD = 'PAket hemat'
    TAB_ENG_TITFIELD_EDIT = 'PAket hemat BANGET'
    DESC_IND0_PACKAGE = 'PAKET murah'
    DESC_ENG_PACKAGE = 'LOW BADGET'
    MULTYPLIER_DW = '3'
    MULTYPLIER_EDIT = '2'
    TYPE_DW = ''
    ANAK_ANAK = 'Anak-anak'
    DEWASA = ''
    SENIOR = ''
    PERSON_MIN = '2'
    PERSON_MAX = '9'
    INVENTORY_DW = ''
    INVENTORY_LIST = 'Test-QA Tour'
    # PRICE
    MIN_QTY = '3'
    MIN_QTY_EDIT = '5'
    NETT = '10000'
    NETT_EDIT = '30000'
    COMMISION_AMOUNT = ''
    COMMISION_PERCENTAGE = '10'
    COMMISION_PERCENTAGE_EDIT = '5'
    TAB_INDO_PRICEPER = ''
    TAB_ENG_PRICEPER = ''
    PRICEPER_INDO = ''
    PRICEPER_ENG = ''
    DISPLAY_PRICE = '30000'
    DISPLAY_PRICE_EDIT = '50000'
    CEBOX_START_TIME = ''
    # CONTENT TABS
    TAB_INDO_INFO = ''
    TAB_ENG_INFO = ''
    INFORMATION_INDO = 'INDONESIAAAAA'
    INFORMATION_INDO_EDIT = 'INFORMASI'
    INFORMATION_ENG = 'INGRESSSSSSS'
    INFORMATION_ENG_EDIT = 'INFORMATION_ENG'
    TAB_INDO_JADWAL = ''
    TAB_ENG_JADWAL = ''
    JADWAL_PEJALANAN_INDO = 'INDO'
    JADWAL_PEJALANAN_INDO_EDIT = 'JADWAL PERJALANAN'
    JADWAL_PEJALANAN_ENG = 'ENGG'
    JADWAL_PEJALANAN_ENG_EDIT = 'Travel Itinerary'
    TAB_INDO_HARGATER = ''
    TAB_ENG_HARGATER = ''
    HARGA_TERMASUK_INDO = 'INDO'
    HARGA_TERMASUK_ENG = 'ENGG'
    HARGA_TERMASUK_INDO_EDIT = 'HARGA TERMASUK'
    HARGA_TERMASUK_ENG_EDIT = 'Prices Included'
    TAB_INDO_HARGATDK = ''
    TAB_ENG_HARGATDK = ''
    HARGA_TDKTERMASUK_INDO = 'INDO'
    HARGA_TDKTERMASUK_INDO_EDIT = 'HARGA TIDAK TERMASUK'
    HARGA_TDKTERMASUK_ENG = 'ENGG'
    HARGA_TDKTERMASUK_ENG_EDIT = ' Prices Not Included'
    TAB_INDO_SYARAT = ''
    TAB_ENG_SYARAT = ''
    SYARAT_KETENTUAN_INDO = 'INDO'
    SYARAT_KETENTUAN_ENG = 'ENGG'
    SYARAT_KETENTUAN_INDO_EDIT = 'Syarat dan Ketentuan '
    SYARAT_KETENTUAN_ENG_EDIT = 'Terms and Conditions'
    # CONTENT ON  E-VOUCHER
    TAB_INDO_INSTRUKSI = ''
    TAB_ENG_INSTRUKSI = ''
    INTRUKSI_INDO = 'INDO'
    INTRUKSI_ENG = 'ENGG'
    INTRUKSI_INDO_EDIT = 'Intruksi Tiket'
    INTRUKSI_ENG_EDIT = 'Ticket Instructions'
    TAB_INDO_SYARATVOUCHER = ''
    TAB_ENG_SYARATVOUCHER = ''
    SYARAT_KETENTUAN_VOCERINDO = 'INDO'
    SYARAT_KETENTUAN_VOCERENG = 'ENG'
    SYARAT_KETENTUAN_VOCERINDO_EDIT = 'Syarat dan Ketentuan'
    SYARAT_KETENTUAN_VOCERENG_EDIT = ' Terms and Conditions'

    #untuk role dan mercnhat user hanya bisa diakses menggunakan url admin ma
    #data Filter user merchant
    MERCHANT_NAME_DW = 'Tour RF'
    FULL_NAME = 'Mister A'
    EMAIL_USER = 'email@gmail.com'
    ROLE = 'Administrator'
    STATUS = 'Active'

    #data invalid user merchant
    FULL_NAME_INVALID = 'abcd'
    EMAIL_USER_INVALID = 'user_gmail.com'

    #DATA ADD USER MERCHANT VALID
    MERCHANTNAME_USER = 'Tour RF'
    FULL_NAMEUSER = 'admin 01'
    EMAIL_USERMERC = 'admin01@gmail.com'
    ROLE_USER = 'Partner Superuser'

    # DATA ADD USER MERCHANT VALID
    ALERT_MERCHANTNAME_NOSELECT = 'This field is required'
    EMAIL_USERMERC_INVALID = 'admin01_gmail.com'
    EMAIL_USERMERC_EXIST = 'misteraladinqa@gmail.com'
    ALERT_FULLNAME_EMPTY = 'This field is required'
    ALERT_EMAILUSER_INVALID = 'This field must be a valid email'
    ALERT_EMAILUSER_EMPTY = 'This field is required'
    ALERT_ROLE_NOSELECT = 'This field is required'




